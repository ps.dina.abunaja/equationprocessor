package operand;

public class Operand {
    private final double operandVal;

    public Operand(String operandVal) {
        this.operandVal = Double.parseDouble(operandVal);
    }

    public Operand(double operandVal) {
        this.operandVal = operandVal;
    }

    public double getOperandVal() {
        return operandVal;
    }

    public static boolean isNumeric(String num) {
        try {
            Double.parseDouble(num);
            return true;
        } catch (NumberFormatException numberFormatException) {
            return false;
        }
    }

}
