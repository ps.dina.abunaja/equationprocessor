package operand;

public interface EquationParameter {
    String getValue(String value);
}
