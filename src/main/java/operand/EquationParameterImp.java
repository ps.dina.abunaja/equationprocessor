package operand;

import java.util.Scanner;

public class EquationParameterImp implements EquationParameter {
    @Override
    public String getValue(String value) {
        Scanner input = new Scanner(System.in);
        value = input.next();
        return value;
    }
}
