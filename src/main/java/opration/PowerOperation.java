package opration;

public class PowerOperation extends Operation {
    @Override
    public double doOperation(double rightNum, double leftNum) {
        return Math.pow(leftNum, rightNum);
    }

    @Override
    public int getPriority() {
        return 3;
    }
}
