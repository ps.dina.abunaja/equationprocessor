package opration;

public abstract class Operation {

    public abstract double doOperation(double rightNum, double leftNum);

    public abstract int getPriority();
}
