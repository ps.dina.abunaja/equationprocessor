package opration;

public class AddOperation extends Operation {
    @Override
    public double doOperation(double rightNum, double leftNum) {
        return leftNum + rightNum;
    }

    @Override
    public int getPriority() {
        return 1;
    }
}
