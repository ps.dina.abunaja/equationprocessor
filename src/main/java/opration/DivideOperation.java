package opration;

public class DivideOperation extends Operation {
    @Override
    public double doOperation(double rightNum, double leftNum) {
        if (rightNum != 0)
            return leftNum / rightNum;
        else {
            throw new ArithmeticException("Cannot divide by zero");
        }
    }

    @Override
    public int getPriority() {
        return 2;
    }
}
