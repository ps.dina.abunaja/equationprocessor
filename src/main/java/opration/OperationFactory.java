package opration;

import java.util.HashMap;
import java.util.Map;

public class OperationFactory {
    private final Map<String, Operation> operations;

    public OperationFactory() {
        operations = new HashMap<>();
        operations.put("+", new AddOperation());
        operations.put("-", new SubtractOperation());
        operations.put("*", new MultiplyOperation());
        operations.put("/", new DivideOperation());
        operations.put("^", new PowerOperation());
        operations.put("(", new LeftParenthesis());
        operations.put(")", new RightParenthesis());
    }

    public Operation createOperation(String operator) {
        return operations.get(operator);
    }

    public boolean isOperation(String operator) {
        return operations.containsKey(operator);
    }

}
