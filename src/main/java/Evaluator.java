import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import operand.EquationParameter;
import operand.Operand;
import opration.Operation;
import opration.OperationFactory;

public class Evaluator {
    private final Stack<Operand> operands;
    private final Stack<Operation> operations;
    private List<String> tokens;
    private final EquationParameter parameter;
    private final OperationFactory operationFactory;

    public Evaluator(EquationParameter parameter) {
        this.operationFactory = new OperationFactory();
        operands = new Stack<>();
        operations = new Stack<>();
        this.parameter = parameter;
    }

    public double evaluate(String expression) {
        giveSignToOperand(expression);
        tokens.stream().filter(getEquationParametersValue()).collect(Collectors.toList());
        for (String token : tokens) {
            checkValue(token);
        }
        return getFinalResult();
    }

    private void giveSignToOperand(String expression) {
        tokens = Arrays.asList(expression.split(" "));
        for (int i = 0; i < tokens.size(); i++) {
            if (tokens.get(i).equals("-") && Operand.isNumeric(tokens.get(i + 1))) {
                tokens.set(i, "+");
                tokens.set(i + 1, "-" + tokens.get(i + 1));
            }
        }
    }

    public Predicate<String> getEquationParametersValue() {
        return token -> {
            if (!operationFactory.isOperation(token) && !Operand.isNumeric(token)) {
                System.out.println("Please enter the value of: " + token);
                return Collections.replaceAll(tokens, token, parameter.getValue(token));
            }
            return false;
        };
    }

    private void checkValue(String value) {
        if (Operand.isNumeric(value)) {
            operands.push(new Operand(value));
        } else if (operationFactory.isOperation(value)) {
            doHigherPriorityOperation(value);
        }
    }

    private void doOperationsInsideParenthesis() {
        while (operations.peek().getPriority() != 0) {
            operands.push(getOperationResult());
        }
        operations.pop();
    }

    private void doHigherPriorityOperation(String value) {
        if (")".equals(value)) {
            doOperationsInsideParenthesis();
        } else if ("(".equals(value)) {
            operations.push(operationFactory.createOperation(value));
        } else {
            Operation newOperation = operationFactory.createOperation(value);
            if (!operations.isEmpty() && operations.peek().getPriority() >= newOperation.getPriority()) {
                operands.push(getOperationResult());
            }
            operations.push(newOperation);
        }
    }

    private double getFinalResult() {
        operands.push(getOperationResult());
        if (!operations.isEmpty())
            getFinalResult();
        return operands.peek().getOperandVal();
    }

    private Operand getOperationResult() {
        Operation olderOp = operations.pop();
        double rightNum = operands.pop().getOperandVal();
        double leftNum = operands.pop().getOperandVal();
        return new Operand(olderOp.doOperation(rightNum, leftNum));
    }
}
