import java.util.Scanner;
import operand.EquationParameter;
import operand.EquationParameterImp;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter equation");
        String equation = input.nextLine();
        EquationParameter parameter = new EquationParameterImp();
        Evaluator evaluator = new Evaluator(parameter);

        System.out.println(evaluator.evaluate(equation));
    }


}
