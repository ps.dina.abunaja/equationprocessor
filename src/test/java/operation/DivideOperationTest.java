package operation;

import opration.DivideOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DivideOperationTest {
    DivideOperation divideOpl;

    @BeforeEach
    void setUp() {
        divideOpl = new DivideOperation();
    }

    @Test
    void testDivideOperationWithFraction() {
        Assertions.assertEquals(2.5, divideOpl.doOperation(2.0, 5.0));
    }

    @Test
    void SimpleDivideOperation() {
        Assertions.assertEquals(25.0, divideOpl.doOperation(5.0, 125.0));
    }

    @Test
    void divideByZeroShouldThrowException() {
        ArithmeticException exception = Assertions.assertThrows(ArithmeticException.class,
                () -> divideOpl.doOperation(0, 124));
        Assertions.assertEquals("Cannot divide by zero", exception.getMessage());
    }
}
