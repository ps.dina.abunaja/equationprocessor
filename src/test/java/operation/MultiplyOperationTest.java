package operation;

import opration.MultiplyOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MultiplyOperationTest {
    MultiplyOperation multiplyOp;

    @BeforeEach
    void setUp() {
        multiplyOp = new MultiplyOperation();
    }

    @Test
    void SimpleMultiplyOperation() {
        Assertions.assertEquals(35.0, multiplyOp.doOperation(7.0, 5.0));
    }

    @Test
    void TestMultiplyOperationWithFraction() {
        Assertions.assertEquals(9.245, multiplyOp.doOperation(2.15, 4.3));
    }

    @Test
    void MultiplyWithZero() {
        Assertions.assertEquals(0.0, multiplyOp.doOperation(5.0, 0));
    }
}
