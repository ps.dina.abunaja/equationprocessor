package operation;

import opration.AddOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AddOperationTest {
    AddOperation addOp;

    @BeforeEach
    void setUp() {
        addOp = new AddOperation();
    }

    @Test
    void testAddOperation() {
        Assertions.assertEquals(11.0, addOp.doOperation(5.0, 6.0));
        Assertions.assertEquals(23.40, addOp.doOperation(15.9, 7.50));
    }
}
