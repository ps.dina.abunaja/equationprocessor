package operation;

import opration.SubtractOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SubtractOperationTest {
    SubtractOperation subtractOp;

    @BeforeEach
    void setUp() {
        subtractOp = new SubtractOperation();
    }

    @Test
    void SimpleSubtractOperation() {
        Assertions.assertEquals(5.0, subtractOp.doOperation(2.0, 7.0));
    }

    @Test
    void SubtractNegativeNumbers() {
        Assertions.assertEquals(-2.5, subtractOp.doOperation(-5.0, -7.5));
    }

    @Test
    void SubtractDifferentSignNumbers() {
        Assertions.assertEquals(12.0, subtractOp.doOperation(-9.0, 3));
    }
}
