package operation;

import opration.PowerOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PowerOperationTest {
    PowerOperation powerOp;

    @BeforeEach
    void setUp() {
        powerOp = new PowerOperation();
    }

    @Test
    void SimplePowerOperation() {
        Assertions.assertEquals(25.0, powerOp.doOperation(2.0, 5.0));
    }

    @Test
    void PowerToOneShouldBeTheSameValue() {
        Assertions.assertEquals(705.0, powerOp.doOperation(1.0, 705.0));
    }

    @Test
    void PowerToZeroShouldBeOne() {
        Assertions.assertEquals(1.0, powerOp.doOperation(0.0, 158.0));

    }
}
