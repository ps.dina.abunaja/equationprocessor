import java.util.HashMap;
import java.util.Map;
import operand.EquationParameter;

public class MockEquationParameter implements EquationParameter {
    Map<String, String> parameters;

    public MockEquationParameter() {
        parameters = new HashMap<>();
    }

    @Override
    public String getValue(String value) {
        return parameters.get(value);
    }

    public void setValue(String var, String val) {
        parameters.put(var, val);
    }
}
