import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EvaluatorTest {
    Evaluator evaluator;
    MockEquationParameter parameter = new MockEquationParameter();

    @BeforeEach
    void setUp() {
        evaluator = new Evaluator(parameter);
    }

    @Test
    void SimpleExpressionResultWithSamePriority() {
        String exp = "5 + 4 + 3 - 5";
        assertEquals(7.0, evaluator.evaluate(exp));
        exp = "5 * 2 / 5";
        assertEquals(2.0, evaluator.evaluate(exp));
    }

    @Test
    void SimpleExpressionResultWithDifferentPriority() {
        String exp = "5 * 2 + 20 / 5 - 4";
        assertEquals(10.0, evaluator.evaluate(exp));

    }

    @Test
    void ExpressionWithParenthesis() {
        String exp = "31 - ( 7 * 2 ) + ( 4 - 3 * ( 2 + 7 ) )";
        assertEquals(-6.0, evaluator.evaluate(exp));
    }

    @Test
    void ExpressionIncludeMultipleMultiplyWithZero() {
        String exp = "5 - 6 * 0 * 6 - 2";
        assertEquals(3.0, evaluator.evaluate(exp));
    }

    @Test
    void ExpressionWithSingleVariable() {
        String exp = "10 - x + 3 * x";
        parameter.setValue("x", "5");
        assertEquals(20.0, evaluator.evaluate(exp));
    }

    @Test
    void ExpressionWithPowerOperation() {
        String exp = "3 * x ^ y";
        parameter.setValue("x", "5");
        parameter.setValue("y", "3");
        assertEquals(375.0, evaluator.evaluate(exp));
    }

    @Test
    void ExpressionWithMultipleVariable() {
        String exp = "3 * x + 2 - y ^ z";
        parameter.setValue("x", "5");
        parameter.setValue("y", "3");
        parameter.setValue("z", "2");
        assertEquals(8.0, evaluator.evaluate(exp));
    }
}